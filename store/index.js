export const state = () => ({
  posts: [],
  meta: {}
})

export const mutations = {
  setPosts(state, data) {
    state.posts = data
  },
  setMeta(state, data) {
    state.meta = data
  }
}

export const actions = {
  async nuxtServerInit({ commit, context }, { app }) {
    const auth = await app.$axios.post('http://michal-backend.cba.pl/graphql', {
      query: `mutation LoginUser {
        login( input: {
          clientMutationId:"uniqueId"
          username: "admin"
          password: "admin"
        } ) {
          authToken
          user {
            id
            name
          }
        }
      }`
    })
    const res = await app.$axios.post('http://michal-backend.cba.pl/graphql', {
      query: `query {
            posts (last: 20) {
            edges {
              node {
                title,
                slug,
                content,
                categories{
                  nodes{
                    name
                  }
                }
              }
            }
          },
          generalSettings {
            description,
            title
          }
        }`

    }, {
      headers: {
        'authorization': `Bearer ${auth.data.data.login.authToken}`
      }
    })

    await commit('setPosts', res.data.data.posts.edges)
    await commit('setMeta', res.data.data.generalSettings)
  }

}
